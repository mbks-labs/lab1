﻿#ifndef _get_file_info_H
#define _get_file_info_H

#include "WinHeaders.h"
#include "ErrorHandler.h"
#include <string>
#include <unordered_map>

enum FILEINT {
	_F_INT_NOVAL,
	F_NO,
	F_UN,
	F_LOW,
	F_MED,
	F_HIGH,
	F_SYS,
	F_PR
};

const std::unordered_map<DWORD, std::string> rightsConvertor{
	{FILE_READ_DATA, "FILE_READ_DATA"},
	{FILE_WRITE_DATA, "FILE_WRITE_DATA"},
	{FILE_APPEND_DATA, "FILE_APPEND_DATA"},
	{FILE_READ_EA, "FILE_READ_EA"},
	{FILE_WRITE_EA, "FILE_WRITE_EA"},
	{FILE_EXECUTE, "FILE_EXECUTE"},
	{FILE_READ_ATTRIBUTES, "FILE_READ_ATTRIBUTES"},
	{FILE_WRITE_ATTRIBUTES, "FILE_WRITE_ATTRIBUTES"},
	{DELETE, "DELETE"},
	{READ_CONTROL, "READ_CONTROL"},
	{WRITE_DAC, "WRITE_DAC"},
	{WRITE_OWNER, "WRITE_OWNER"},
	{SYNCHRONIZE, "SYNCHRONIZE"},
	{GENERIC_READ, "GENERIC_READ"},
	{GENERIC_WRITE, "GENERIC_WRITE"},
	{GENERIC_EXECUTE, "GENERIC_EXECUTE"}
};

DWORD rightConvert(std::string right);

std::string toString(FILEINT& fInt);

class FileInformation {
private:
	std::string filepath;
	std::string ownerSid;
	FileInformation() = delete;
	HANDLE fileHandle = INVALID_HANDLE_VALUE;
	FILEINT integrity = _F_INT_NOVAL;

	std::unordered_map<std::string, DWORD> rights;
	bool parseDACL(PACL dacl);
public:
	FileInformation(std::string& filepath);

	bool getOwnerAndDACL();
	bool getIntegrity();

	void printInfo();
	jsonxx::Array toJSON();

	bool setOwner(std::string newOwnerSID);
	bool setIntegrity(FILEINT intLvl);
	bool setRights(std::string sid, DWORD rights, bool setNotRemove = true);
};

#endif