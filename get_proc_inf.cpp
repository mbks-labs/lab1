﻿#include "get_proc_info.h"

#include "ErrorHandler.h"
#include <string>
#include <algorithm>
#include <iomanip>

ProcessInformation::ProcessInformation(){
	this->initialized = false;
	this->processHandle = INVALID_HANDLE_VALUE;
	this->processName = "";
	this->processDesc = "";
	this->processID = ULONG_MAX;
	this->processExePath = "";
	this->processParentName = "";
	this->processParentID = ULONG_MAX;
	this->processType = _TYPE_NO_VAL;
	this->processEnvironment = _ENV_NO_VAL;
	this->processUseDEP = false;
	this->processUseASLR = false;
	this->processModules = std::vector<std::string>();
	this->processIntegrityLevel = _INT_NO_VAL;
	this->processPrivileges = std::vector<std::pair<LUID, bool> >();
}

std::string getStrFromLuid(LUID luid) {
	TCHAR privName[512];
	DWORD privNameSize = sizeof(privName);
	if (!LookupPrivilegeName(NULL, &luid, privName, &privNameSize)) {
		printError(L"LookupPrivilegeName");
		return "<err>";
	}
	return std::string(privName);
}

ProcessInformation::~ProcessInformation()
{
	
}

ProcessInformation::ProcessInformation(PROCESSENTRY32 pe32) : ProcessInformation() {
	this->processID = pe32.th32ProcessID;
	this->processParentID = pe32.th32ParentProcessID;
	this->processName = std::string(pe32.szExeFile);

	if (INVALID_HANDLE_VALUE == (this->processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, this->processID))) {
		this->processHandle = INVALID_HANDLE_VALUE;
		printError(L"OpenProcess<<<<<");
	}

	this->findExePath();
	this->findDescription();
	this->findParentName();
	this->findType();
	this->findModules();
	this->findEnv();
	this->findDEPPolicy();
	this->findASLRPolicy();
	this->findIntegrity();
	this->findPrivileges();
	this->findOwner();

	this->initialized = true;

	CloseHandle(this->processHandle);
}

ProcessInformation::ProcessInformation(int PID) : ProcessInformation() {
	this->processID = static_cast<DWORD>(PID);

	if (INVALID_HANDLE_VALUE == (this->processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, this->processID))) {
		this->processHandle = INVALID_HANDLE_VALUE;
		printError(L"OpenProcess<<<<<");
	}

	this->findExePath();
	this->findDescription();
	this->findType();
	this->findModules();
	this->findEnv();
	this->findDEPPolicy();
	this->findASLRPolicy();
	this->findIntegrity();
	this->findPrivileges();

	this->initialized = true;

	CloseHandle(this->processHandle);
}

void ProcessInformation::printInfo()
{
	if (!initialized)
		return;

	std::cout <<
		"PID: " << this->processID << std::endl <<
		"Name: " << this->processName << std::endl <<
		"DESC: " << (this->processDesc.size() == 0 ? this->processName : this->processDesc) << std::endl <<
		"PPID: " << this->processParentID << std::endl <<
		"DEP: " << (this->processUseDEP ? "TRUE" : "FALSE") << std::endl <<
		"ASLR: " << (this->processUseASLR ? "TRUE" : "FALSE") << std::endl <<
		"INTEGRITY: " << this->processIntegrityLevel << std::endl <<
		"PARENT NAME: " << this->processParentName << std::endl <<
		"PROCESS TYPE: " << (this->processType == PROCTYPE::x64 ? "x64" : "x86") << std::endl <<
		"PROCESS ENV: " << (this->processEnvironment == PROCENV::CLR ? "CLR" : "NATIVE") << std::endl <<
		"PRIVILEGES: " << std::endl;
	for (auto luid_pair : this->processPrivileges) {
		auto luid = luid_pair.first;
		TCHAR privName[512];
		DWORD privNameSize = sizeof(privName);
		if (!LookupPrivilegeName(NULL, &luid, privName, &privNameSize)) {
			printError(L"LookupPrivilegeName");
			continue;
		}
		std::cout << ">>" << std::left << std::setw(40) << privName  << std::setw(10) << std::right << (luid_pair.second ? "+" : "-") << std::endl;
	}
	std::cout << "MODULES: " << std::endl;
	for (int i = 0; i < this->processModules.size(); ++i) {
		std::cout << this->processModules[i] << std::endl;
	}

	std::cout << "===" << std::endl;
}

bool ProcessInformation::findDescription()
{	
	if (this->processHandle == INVALID_HANDLE_VALUE) return false;
	
	HRESULT hr;

	struct LANGANDCODEPAGE {
		WORD wLanguage;
		WORD wCodePage;
	} *lpTranslate;

	const TCHAR* filename = this->processExePath.c_str();
	DWORD infoSize = 0;

	if (!(infoSize = GetFileVersionInfoSize(filename, NULL))) {
		printError(L"GetFileVersionInfoSize");
		return false;
	}

	LPVOID pBlock = malloc(infoSize);

	if (!GetFileVersionInfo(filename, 0, infoSize, pBlock)) {
		printError(L"GetFileVersionInfo");
		free(pBlock);
		return false;
	}

	UINT cbTranslate;
	TCHAR SubBlock[50];
	LPVOID lpBuffer;
	UINT dwBytes;


	// Read the list of languages and code pages.

	VerQueryValue(pBlock,
		TEXT("\\VarFileInfo\\Translation"),
		(LPVOID*)&lpTranslate,
		&cbTranslate);

	// Read the file description for each language and code page.
	
	for (int i = 0; i < (cbTranslate / sizeof(struct LANGANDCODEPAGE)); i++)
	{
		hr = StringCchPrintf(SubBlock, 50,
			TEXT("\\StringFileInfo\\%04x%04x\\FileDescription"),
			lpTranslate[i].wLanguage,
			lpTranslate[i].wCodePage);
		if (FAILED(hr))
		{
			printError(L"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", 0);
		}

		// Retrieve file description for language and code page "i". 
		VerQueryValue(pBlock,
			SubBlock,
			&lpBuffer,
			&dwBytes);
	
		TCHAR* safeBuffer = (TCHAR*)malloc(dwBytes * sizeof(TCHAR));
		memset(safeBuffer, 0, dwBytes * sizeof(TCHAR));
		strncpy(safeBuffer, (LPTSTR)lpBuffer, dwBytes);
		this->processDesc = std::string(safeBuffer);
		if (this->processDesc[0] >= 0xFD) this->processDesc = "";
		free(safeBuffer);
	}

	free(pBlock);
	return true;
}

bool ProcessInformation::findParentName()
{
	HANDLE parentHandle;
	if (!(parentHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, this->processParentID))) {
		this->processHandle = INVALID_HANDLE_VALUE;
		printError(L"OpenProcess");
	}

	TCHAR filename[MAX_PATH];
	if (!GetModuleFileNameEx(parentHandle, 0, filename, MAX_PATH)) {
		printError(L"GetModuleFileNameEx");
		return false;
	}

	std::string parentFilename = filename;
	std::size_t pExeStart = parentFilename.rfind("\\");
	this->processParentName = parentFilename.substr(pExeStart + 1);

	return true;
}

bool ProcessInformation::findType()
{
	DWORD binType = 0;
	if (!GetBinaryTypeA((LPCSTR)(this->processExePath.c_str()), &binType)) {
		printError(L"GetBinaryTypeA");
		return false;
	}

	if (binType == SCS_32BIT_BINARY)
		this->processType = PROCTYPE::x86;
	else if (binType == SCS_64BIT_BINARY)
		this->processType = PROCTYPE::x64;
	else
		this->processType = PROCTYPE::OTHER;

	return true;
}

bool ProcessInformation::findEnv()
{
	std::string clrReqName = "mscoree.dll";
	for (auto mod : this->processModules) {
		size_t modTruncFirst = mod.rfind('\\') + 1;
		std::string modTrunc = mod.substr(modTruncFirst);
				
		std::string modCopy; modCopy.resize(modTrunc.size());
		std::transform(modTrunc.begin(), modTrunc.end(), modCopy.begin(), tolower);
		if (modCopy.compare(clrReqName) == 0) {
			this->processEnvironment = PROCENV::CLR;
			return true;
		}
	}
	this->processEnvironment = PROCENV::NATIVE;
	return true;
}

bool ProcessInformation::findDEPPolicy()
{
	if (this->processHandle == INVALID_HANDLE_VALUE)
		return false;

	DWORD flags; BOOL perm;

	if (!GetProcessDEPPolicy(this->processHandle, &flags, &perm)) {
		printError(L"GetProcessDEPPolicy");
		return false;
	}

	this->processUseDEP = (flags & PROCESS_DEP_ENABLE);
	return true;
}

bool ProcessInformation::findASLRPolicy()
{
	if (this->processHandle == INVALID_HANDLE_VALUE)
		return false;

	PROCESS_MITIGATION_ASLR_POLICY policy;

	if (!GetProcessMitigationPolicy(this->processHandle, PROCESS_MITIGATION_POLICY::ProcessASLRPolicy, &policy, sizeof(policy))) {
		printError(L"GetProcessMitigationPolicy");
		return false;
	}

	this->processUseASLR = (policy.Flags != 0);
	return false;
}

bool ProcessInformation::findModules()
{
	if (this->processHandle == INVALID_HANDLE_VALUE)
		return false;

	HMODULE hMods[1024];
	DWORD cbNeeded;

	if (!EnumProcessModules(this->processHandle, hMods, sizeof(hMods), &cbNeeded)) {
		printError(L"EnumModules");
		return false;
	}
	
	for (int i = 0; i < (cbNeeded / sizeof(HMODULE)); ++i) {
		TCHAR szModuleName[MAX_PATH];

		if (!GetModuleFileNameEx(this->processHandle, hMods[i], szModuleName, MAX_PATH)) {
			printError(L"GetModuleFileNameEx");
			return false;
		}
		this->processModules.push_back(std::string(szModuleName));
	}

	return true;
}

bool ProcessInformation::findIntegrity()
{
	HANDLE procToken;
	if (!OpenProcessToken(this->processHandle, TOKEN_QUERY, &procToken)) {
		printError(L"OpenProcessToken");
		return false;
	}

	this->processIntegrityLevel = PROCINTEGRITY::UNKNOWN_INTEGRITY;

	DWORD tokenInfoLen = 0;
	if (GetTokenInformation(procToken, TokenIntegrityLevel, NULL, 0, &tokenInfoLen) || GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
		printError(L"GetTokenInformation 1");
		return false;
	}
	
	TOKEN_MANDATORY_LABEL* tokenLabel = (TOKEN_MANDATORY_LABEL*)malloc(tokenInfoLen);
	if (!GetTokenInformation(procToken, TokenIntegrityLevel, tokenLabel, tokenInfoLen, &tokenInfoLen)) {
		printError(L"GetTokenInformation 2");
		free(tokenLabel);
		return false;
	}

	DWORD integrityLevel = *GetSidSubAuthority(tokenLabel->Label.Sid, (DWORD)(*GetSidSubAuthorityCount(tokenLabel->Label.Sid) - 1));
		
	if (integrityLevel < SECURITY_MANDATORY_LOW_RID)
		this->processIntegrityLevel = PROCINTEGRITY::UNTRUSTED_INTEGRITY;
	else if(integrityLevel  < SECURITY_MANDATORY_MEDIUM_RID)
		this->processIntegrityLevel = PROCINTEGRITY::LOW_INTEGRITY;
	else if (integrityLevel >= SECURITY_MANDATORY_MEDIUM_RID &&
				integrityLevel < SECURITY_MANDATORY_HIGH_RID)
		this->processIntegrityLevel = PROCINTEGRITY::MEDIUM_INTEGRITY;
	else if (integrityLevel >= SECURITY_MANDATORY_HIGH_RID && 
				integrityLevel < SECURITY_MANDATORY_SYSTEM_RID)
		this->processIntegrityLevel = PROCINTEGRITY::HIGH_INTEGRITY;
	else if (integrityLevel >= SECURITY_MANDATORY_SYSTEM_RID && 
				integrityLevel < SECURITY_MANDATORY_PROTECTED_PROCESS_RID)
		this->processIntegrityLevel = PROCINTEGRITY::SYSTEM_INTEGRITY;
	else if (integrityLevel >= SECURITY_MANDATORY_PROTECTED_PROCESS_RID)
		this->processIntegrityLevel = PROCINTEGRITY::PROTECTED_INTEGRITY;

	free(tokenLabel);
	CloseHandle(procToken);
	return true;
}

bool ProcessInformation::setIntegrity(PROCINTEGRITY intLvl)
{
	if (INVALID_HANDLE_VALUE == (this->processHandle = OpenProcess(PROCESS_SET_INFORMATION | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, this->processID))) {
		this->processHandle = INVALID_HANDLE_VALUE;
		printError(L"^^^^^^^^^^^^^^^^ OpenProcess");
		return false;
	}

	ImpersonateSelf(SECURITY_IMPERSONATION_LEVEL::SecurityImpersonation);

	HANDLE procToken;
	if (!OpenProcessToken(this->processHandle, TOKEN_ALL_ACCESS , &procToken)) {
		printError(L"OpenProcessToken ^^^^^^^^^^^^");
		return false;
	}

	DWORD tokenInfoLen = 0;
	if (GetTokenInformation(procToken, TokenIntegrityLevel, NULL, 0, &tokenInfoLen) || GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
		printError(L"GetTokenInformation 1");
		return false;
	}

	TOKEN_MANDATORY_LABEL* tokenLabel = (TOKEN_MANDATORY_LABEL*)calloc(1, sizeof(TOKEN_MANDATORY_LABEL));
	if (!GetTokenInformation(procToken, TokenIntegrityLevel, tokenLabel, tokenInfoLen, &tokenInfoLen)) {
		printError(L"GetTokenInformation 2");
		free(tokenLabel);
		return false;
	}

	tokenLabel->Label.Attributes = SE_GROUP_INTEGRITY;
	PSID sid;
	if (!ConvertStringSidToSidA(integritySIDs.at(intLvl).c_str(), &sid)) {
		printError(L"ConvertStringSidToSidA");
	}
	tokenLabel->Label.Sid = sid;

	if (!SetTokenInformation(procToken, TokenIntegrityLevel, tokenLabel, tokenInfoLen)) {
		printError(L"SetTokenInformation");
		return false;
	}
	
	free(tokenLabel);
	return true;
}

std::string procsidToString(PSID sid) {
	LPTSTR sidStr = NULL;
	if (!ConvertSidToStringSid(sid, &sidStr)) {
		printError(L"ConvertSidToStringSid");
		return "<error>";
	}
	std::string ret(sidStr);
	return ret;
}

bool ProcessInformation::findOwner() {
	if (this->processHandle == INVALID_HANDLE_VALUE)
		return false;

	HANDLE procToken;
	if (!OpenProcessToken(this->processHandle, TOKEN_QUERY, &procToken)) {
		printError(L"OpenProcessToken");
		return false;
	}

	DWORD tokenInfoLen = 0;
	if (GetTokenInformation(procToken, TokenOwner, NULL, 0, &tokenInfoLen) || GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
		printError(L"GetTokenInformation 1");
		return false;
	}

	TOKEN_OWNER* tokenOwner = (TOKEN_OWNER*)malloc(tokenInfoLen);
	if (!GetTokenInformation(procToken, TokenOwner, tokenOwner, tokenInfoLen, &tokenInfoLen)) {
		printError(L"GetTokenInformation 2");
		free(tokenOwner);
		return false;
	}

	this->ownerSID = procsidToString(tokenOwner->Owner);
	free(tokenOwner);
	return true;
}

bool ProcessInformation::findPrivileges()
{
	if (this->processHandle == INVALID_HANDLE_VALUE)
		return false;

	HANDLE procToken;/*
	std::cout << ImpersonateSelf(SECURITY_IMPERSONATION_LEVEL::SecurityImpersonation);*/
	if (!OpenProcessToken(this->processHandle, TOKEN_QUERY, &procToken)) {
		printError(L"OpenProcessToken ####################");
		return false;
	}

	DWORD tokenInfoLen = 0;
	if (GetTokenInformation(procToken, TokenPrivileges, NULL, 0, &tokenInfoLen) || GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
		printError(L"GetTokenInformation 1");
		return false;
	}

	TOKEN_PRIVILEGES* tokenPrivileges = (TOKEN_PRIVILEGES*)malloc(tokenInfoLen);
	if (!GetTokenInformation(procToken, TokenPrivileges, tokenPrivileges, tokenInfoLen, &tokenInfoLen)) {
		printError(L"GetTokenInformation 2");
		free(tokenPrivileges);
		return false;
	}

	for (int i = 0; i < tokenPrivileges->PrivilegeCount; ++i) {
		LUID_AND_ATTRIBUTES currPrivLUID = tokenPrivileges->Privileges[i];
		this->processPrivileges.push_back(std::make_pair(currPrivLUID.Luid, currPrivLUID.Attributes != 0));
	}

	free(tokenPrivileges);
	CloseHandle(procToken);
	return true;
}

bool ProcessInformation::findExePath()
{
	if (this->processHandle == INVALID_HANDLE_VALUE)
		return false;

	TCHAR filename[MAX_PATH];
	if (!GetModuleFileNameEx(this->processHandle, 0, filename, MAX_PATH)) {
		printError(L"GetModuleFileNameEx");
		return false;
	}

	this->processExePath = std::string(filename);
	
	return true;
}

std::string toString(const PROCTYPE& type) {
	switch (type) {
	case _TYPE_NO_VAL:
		return ("");
		break;
	case x86:
		return ("x86");
		break;
	case x64:
		return ("x64");
		break;
	case OTHER:
		return ("unknown");
		break;
	}
}

std::string toString(const PROCENV& env) {
	switch (env) {
	case _ENV_NO_VAL:
		return ("");
		break;
	case NATIVE:
		return ("Native");
		break;
	case CLR:
		return ("CLR");
		break;
	}
}


std::string toString(const PROCINTEGRITY& i) {
	switch (i) {
	case _INT_NO_VAL:
		return "";
	case UNKNOWN_INTEGRITY:
		return ("unknown");
		break;
	case UNTRUSTED_INTEGRITY:
		return ("untrusted");
		break;
	case LOW_INTEGRITY:
		return ("low");
		break;
	case MEDIUM_INTEGRITY:
		return ("medium");
		break;
	case HIGH_INTEGRITY:
		return ("high");
		break;
	case SYSTEM_INTEGRITY:
		return ("system");
		break;
	case PROTECTED_INTEGRITY:
		return ("protected");
		break;
	}
}

jsonxx::Object ProcessInformation::toJSON()
{
	jsonxx::Object proc;
	proc << "PID" << this->processID;
	proc << "name" << this->processName;
	proc << "desc" << this->processDesc;
	proc << "exe" << this->processExePath;
	proc << "PPID" << this->processParentID;
	proc << "parent_name" << this->processParentName;
	proc << "type" << toString(this->processType);
	proc << "env" << toString(this->processEnvironment);
	proc << "DEP" << this->processUseDEP;
	proc << "ASLR" << this->processUseASLR;
	proc << "integrity" << toString(this->processIntegrityLevel);
	proc << "owner" << this->ownerSID;

	jsonxx::Array modules;
	for (auto mod : this->processModules) {
		modules << mod;
	}
	proc << "modules" << modules;

	jsonxx::Array privs;
	for (auto priv : this->processPrivileges) {
		jsonxx::Object privilege;
		privilege << "name" << getStrFromLuid(priv.first);
		privilege << "value" << priv.second;

		privs << privilege;
	}

	proc << "privileges" << privs;

	return proc;
}

BOOL SetPrivilege(
	HANDLE hToken,          // access token handle
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;

	if (!LookupPrivilegeValue(
		NULL,            // lookup privilege on local system
		lpszPrivilege,   // privilege to lookup 
		&luid))        // receives LUID of privilege
	{
		printError(L"LookupPrivilegeValue");
		return FALSE;
	}

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	if (bEnablePrivilege)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	// Enable the privilege or disable all privileges.

	if (!AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		(PTOKEN_PRIVILEGES)NULL,
		(PDWORD)NULL))
	{
		printError(L"AdjustTokenPrivileges");
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

	{
		printError(L"Not all assigned");
		return FALSE;
	}

	return TRUE;
}

bool ProcessInformation::setPrivilege(std::string se_privilege, bool value)
{
	if (INVALID_HANDLE_VALUE == (this->processHandle = OpenProcess(PROCESS_SET_INFORMATION | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, this->processID))) {
		this->processHandle = INVALID_HANDLE_VALUE;
		printError(L"^^^^^^^^^^^^^^^^ OpenProcess");
		return false;
	}
	
	ImpersonateSelf(SECURITY_IMPERSONATION_LEVEL::SecurityImpersonation);

	HANDLE procToken;
	if (!OpenProcessToken(this->processHandle, TOKEN_ADJUST_PRIVILEGES, &procToken)) {
		printError(L"OpenProcessToken ^^^^^^^^^^^^");
		return false;
	}

	bool res = SetPrivilege(procToken, se_privilege.c_str(), value);
	CloseHandle(procToken);
	return res;
}

std::string ProcessInformation::getName()
{
	return this->processName;
}

unsigned int ProcessInformation::getPid()
{
	return this->processID;
}

std::string ProcessInformation::getPath()
{
	return this->processExePath;
}
