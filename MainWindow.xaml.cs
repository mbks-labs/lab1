﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Management;




namespace mbks
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {

        public bool Genders { get; set; }
        public ObservableCollection<ComboBoxItem> cbItems { get; set; }
        public ComboBoxItem SelectedcbItem { get; set; }
        Dictionary<int, string> int_levels = new Dictionary<int, string>(6);
        Dictionary<string, string> all_sids = new Dictionary<string, string>();
        List<sid> sid_list = new List<sid>();
        string curDir = Directory.GetCurrentDirectory();

        public MainWindow()
        {
            InitializeComponent();
            int_levels.Add(1, "untrusted");
            int_levels.Add(2, "low");
            int_levels.Add(3, "medium");
            int_levels.Add(4, "high");
            int_levels.Add(5, "system");
            int_levels.Add(6, "protected");
        }

        private void TestBut_Click(object sender, RoutedEventArgs e)
        {
            testBut.IsEnabled = false;
            ProcessDataGrid.Items.Clear();
            ProcessDLLDataGrid.Items.Clear();
            ProcessPrivDataGrid.Items.Clear();
            foreach (DataGridColumn column in ProcessDataGrid.Columns)
            {
                column.SortDirection = null;
            }

            /*ничего не надо, тут все классно*/
            /*----------------------------*/
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.UseShellExecute = false;
            //startInfo.RedirectStandardOutput = true;
            startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";
            //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
            startInfo.Arguments = $"-d -o \\procs.json";
            startInfo.Verb = "runas";
            process.StartInfo = startInfo;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.Start();
            //string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            //if (output.IndexOf("Successfully set!", 0) > 0)
            //{ IntegrityComboBox.Text = cur_int; IntegrityComboBox.Foreground = Brushes.Tomato; }
            //else IntegrityComboBox.Text = new_int;

            /*----------------------------*/


            List<TestProcess> result;
            using (StreamReader sr = new StreamReader($"{curDir + "\\procs.json"}", Encoding.GetEncoding(1251)))
            {
                result = JsonConvert.DeserializeObject<List<TestProcess>>(sr.ReadToEnd());
            }

            foreach (TestProcess proc in result)
            {
                ProcessDataGrid.Items.Add(proc);
            }

            testBut.IsEnabled = true;
        }

        private void TestBut2_Click(object sender, RoutedEventArgs e)
        {
            List<TestProcess> list = ProcessDataGrid.Items.OfType<TestProcess>().ToList();

            using (StreamWriter sw = new StreamWriter($"{curDir + "\\procs.json"}", false, System.Text.Encoding.Default))
            {
                var toFile = JsonConvert.SerializeObject(list, Formatting.Indented);
                sw.Write(toFile);
            }
        }

        private void FileOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                string filename = openFileDialog.FileName;
                FileName.Text = filename;
            }
        }

        private void FileName_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effects = DragDropEffects.All;
            }
        }

        private void FileName_Drop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files != null && files.Length != 0 && File.Exists(files[0]))
            {
                FileName.Text = files[0];
            }
        }

        private void FileName_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }

        private void FileDataGrid_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollFiles.ScrollToVerticalOffset(ScrollFiles.VerticalOffset - e.Delta);
        }

        private void ScrollProcess_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var ob = sender as ScrollViewer;
            ob.ScrollToVerticalOffset(ob.VerticalOffset - e.Delta);
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;

        }

        private void ProcessDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                DataGrid grid = sender as DataGrid;
                if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                {
                    var index = grid.Items.IndexOf(grid.CurrentItem);
                    var row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
                    TestProcess currentObject = (TestProcess)row.DataContext;
                    ProcessDLLDataGrid.Items.Clear();
                    ProcessPrivDataGrid.Items.Clear();
                    foreach (string s in currentObject.modules)
                    {
                        var proc = new Process_dll { DLLs = s };
                        ProcessDLLDataGrid.Items.Add(proc);
                    }

                    foreach (Process_priv s in currentObject.privileges)
                    {
                        ProcessPrivDataGrid.Items.Add(s);
                    }

                    IntegrityComboBox.Text = currentObject.integrity;

                }
                
            }
        }

        private void ProcessDataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IntegrityComboBox.Foreground = Brushes.Black;
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            int cur_integ = 0, new_integ = 0;
            TestProcess ob = (TestProcess)ProcessDataGrid.SelectedItem;
            string cur_int = ob.integrity;
            var new_int = selectedItem.Content as string;

            foreach (KeyValuePair<int, string> keyValue in int_levels)
            {
                if (keyValue.Value == cur_int) cur_integ = keyValue.Key;
                if (keyValue.Value == new_int) new_integ = keyValue.Key;
            }


            if (new_integ >= cur_integ) { IntegrityComboBox.Foreground = Brushes.Tomato; }
            else
            {
                /*поменять права у процесса*/
                /*----------------------------*/
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                //startInfo.UseShellExecute = false;
                //startInfo.RedirectStandardOutput = true;
                startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";

                string args = new_int.Substring(0,2).ToUpper();                             //UN|LO|ME|HI|SI|PR добавить к строке аргументс

                startInfo.Arguments = $"-p {ob.PID} -i {args}";//////вот сюда
                startInfo.Verb = "runas";
                process.StartInfo = startInfo;
                process.Start();
                //string output = process.StandardOutput.ReadToEnd();
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.WaitForExit();
                if (process.ExitCode != 0)                          ///проверить правильность вывода консоли
                { IntegrityComboBox.Text = cur_int; IntegrityComboBox.Foreground = Brushes.Tomato; }
                else IntegrityComboBox.Text = new_int;

                /*----------------------------*/

            }
        }

        private void TestBut3_Click(object sender, RoutedEventArgs e)
        {

            FileSIDDataGrid.Items.Clear();
            FilePrivDataGrid.Items.Clear();

            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher("root\\CIMV2",
                "SELECT * FROM Win32_Account");

            

            foreach (ManagementObject queryObj in searcher.Get())
            {
                var temp = new sid { sid1 = (string)queryObj["SID"], username1=(string)queryObj["Name"] };
                sid_list.Add(temp);
                FileOwner.Items.Add(queryObj["Name"]);
                ComboBoxAddNewUser.Items.Add(queryObj["Name"]);
            }



            List<TestFiles> result;

            /*поменять вывод файлика джсон для файла*/
            /*путь к файлику через FileName.Text*/
            /*----------------------------*/
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";
            startInfo.Arguments = $"-f {FileName.Text} -g";  //здесь
            startInfo.Verb = "runas";
            process.StartInfo = startInfo;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.Start();
            process.WaitForExit();

            /*----------------------------*/

            using (StreamReader sr = new StreamReader($"{curDir}\\file.json", Encoding.GetEncoding(1251)))
            {
                result = JsonConvert.DeserializeObject<List<TestFiles>>(sr.ReadToEnd());
            }

            var proc = (TestFiles)result[0];
            FileDataGrid.Items.Add(proc);
            foreach (ACLs acls in proc.rights)
            {
                string name = "";
                foreach(sid i in sid_list)
                {
                    if (i.sid1 == acls.sid) name = i.username1;
                }
                var temp = new sid { sid1 = acls.sid, username1 = name };
                FileSIDDataGrid.Items.Add(temp);

            }

            FileName.Text = result.First().path;
            string[] words = result.First().path.Split('\\');
            FilePath.Text = words.Last();
            string name1 = "";
            foreach (sid i in sid_list)
            {
                if (i.sid1 == result.First().owner) { name1 = i.username1; break; }
            }
            FileOwner.Text = name1;
            FileOwnerSid.Text = result.First().owner;
            FileIntegrity.Text = result.First().integrity;

        }

        private ObservableCollection<Process_priv> _examples;
        public ObservableCollection<Process_priv> Examples
        {
            get
            {
                if (_examples == null)
                    _examples = new ObservableCollection<Process_priv>();
                return _examples;
            }
        }


        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            var index = ProcessDataGrid.Items.IndexOf(ProcessDataGrid.SelectedItem);
            
            var row = (DataGridRow)ProcessDataGrid.ItemContainerGenerator.ContainerFromIndex(index);
            TestProcess currentObject = (TestProcess)row.DataContext;

            var list = new List<Process_priv>();
            foreach (Process_priv i in ProcessPrivDataGrid.Items)
            {
                list.Add(i);
            }

            string integr = IntegrityComboBox.Text;

            currentObject.privileges = list;
            currentObject.integrity = integr;
        }

        void OnChecked(object sender, RoutedEventArgs e)
        {
            if (sender != ProcessDataGrid)
            {
                var index1 = ProcessPrivDataGrid.Items.IndexOf(ProcessPrivDataGrid.CurrentItem);
                if (index1 >= 0)
                {
                    TestProcess currentObject = (TestProcess)ProcessDataGrid.SelectedItem;

                    /*назначить привилегию для процесса*/
                    //для пида - currentObject.PID, для права -  currentObject.privileges[index1].name
                    /*----------------------------*/
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    //startInfo.UseShellExecute = false;
                    //startInfo.RedirectStandardOutput = true;
                    startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";
                    //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
                    startInfo.Arguments = $"-p {currentObject.PID} -se {currentObject.privileges[index1].name}:true";   ////сюда
                    startInfo.Verb = "runas";
                    process.StartInfo = startInfo;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.Start();
                    /*----------------------------*/
                    process.WaitForExit();
                    if (process.ExitCode == 0)
                    currentObject.privileges[index1].value = true;
                    else
                    {
                        var o = sender as CheckBox;
                        o.IsChecked = false;
                    }
                }
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender != ProcessDataGrid)
            {
                var index1 = ProcessPrivDataGrid.Items.IndexOf(ProcessPrivDataGrid.CurrentItem);
                if (index1 >= 0)
                {
                    /*снять привилегию для процесса*/
                    //для пида - currentObject.PID, для права -  currentObject.privileges[index1].name
                    TestProcess currentObject = (TestProcess)ProcessDataGrid.SelectedItem;
                    /*----------------------------*/
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    //startInfo.UseShellExecute = false;
                    //startInfo.RedirectStandardOutput = true;
                    startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";
                    //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
                    startInfo.Arguments = $"-p {currentObject.PID} -se {currentObject.privileges[index1].name}:false";   ////сюда
                    startInfo.Verb = "runas";
                    process.StartInfo = startInfo;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.Start();
                    /*----------------------------*/
                    process.WaitForExit();
                    if (process.ExitCode == 0)
                        currentObject.privileges[index1].value = false;
                    else
                    {
                        var o = sender as CheckBox;
                        o.IsChecked = true;
                    }
                }
            }
        }

        private void FileDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                DataGrid grid = sender as DataGrid;
                if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                {
                    
                    var index = grid.Items.IndexOf(grid.CurrentItem);


                    var row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
                    sid currentUser = (sid)row.DataContext;

                    FilePrivDataGrid.Items.Clear();
                    TestFiles currentObject = (TestFiles)FileDataGrid.Items[0];
                    ACLs currentACL = new ACLs(); 
                    foreach(ACLs i in currentObject.rights)
                    {
                        if (currentUser.sid1 == i.sid)
                        {
                            currentACL = i;
                            break;
                        }
                    }

                    foreach (access_list s in currentACL.access)
                    {
                        FilePrivDataGrid.Items.Add(s);
                    }
                }


            }
        }

        void OnChecked1(object sender, RoutedEventArgs e)
        {
            if (sender != ProcessDataGrid)
            {
                var index1 = FilePrivDataGrid.Items.IndexOf(FilePrivDataGrid.CurrentItem);
                if (index1 >= 0)
                {
                    var index = FileSIDDataGrid.Items.IndexOf(FileDataGrid.SelectedItem);


                    sid currentUser = (sid)FileSIDDataGrid.SelectedItem;
                    var right = ((access_list)FilePrivDataGrid.CurrentItem).name;

                    /*сменить права для файла*/
                    /*для юзера - currentUser.sid1, для права - (access_list)FileSIDDataGrid.CurrentItem .name*/
                    /*----------------------------*/
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    //startInfo.UseShellExecute = false;
                    //startInfo.RedirectStandardOutput = true;
                    startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";
                    //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
                    startInfo.Arguments = $"-f {FileName.Text} -ra {currentUser.sid1} {right}";   ////сюда
                    startInfo.Verb = "runas";
                    process.StartInfo = startInfo;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.Start();
                    ///*----------------------------*/
                    //string output = process.StandardOutput.ReadToEnd();
                    process.WaitForExit();

                    if (process.ExitCode == 0)
                    {

                        TestFiles currentObject = (TestFiles)FileDataGrid.Items[0];
                        ACLs currentACL = new ACLs();
                        foreach (ACLs i in currentObject.rights)
                        {
                            if (currentUser.sid1 == i.sid)
                            {
                                currentACL = i;
                                break;
                            }
                        }
                        
                        currentACL.access[index1].value = true;
                        MessageBox.Show("10");
                    }
                    else
                    {
                        var o = sender as CheckBox;
                        o.IsChecked = false;
                    }
                }
            }
        }

        private void CheckBox_Unchecked1(object sender, RoutedEventArgs e)
        {
            if (sender != ProcessDataGrid)
            {
                var index1 = FilePrivDataGrid.Items.IndexOf(FilePrivDataGrid.CurrentItem);
                if (index1 >= 0)
                {
                    sid currentUser = (sid)FileSIDDataGrid.SelectedItem;

                    var right = ((access_list)FilePrivDataGrid.CurrentItem).name;

                    /*снять права для файла*/
                    /*для юзера - currentUser.sid1, для права - (access_list)FileSIDDataGrid.CurrentItem .name*/
                    /*----------------------------*/
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    //startInfo.UseShellExecute = false;
                    //startInfo.RedirectStandardOutput = true;
                    startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";
                    //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
                    startInfo.Arguments = $"-f {FileName.Text} -rr {currentUser.sid1} {right}";   ////сюда
                    startInfo.Verb = "runas";
                    process.StartInfo = startInfo;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    process.Start();
                    /*----------------------------*/
                    process.WaitForExit();
                    if (process.ExitCode == 0)
                    {

                        TestFiles currentObject = (TestFiles)FileDataGrid.Items[0];
                        ACLs currentACL = new ACLs();
                        foreach (ACLs i in currentObject.rights)
                        {
                            if (currentUser.sid1 == i.sid)
                            {
                                currentACL = i;
                                break;
                            }
                        }

                        currentACL.access[index1].value = false;
                    }
                    else
                    {
                        var o = sender as CheckBox;
                        o.IsChecked = true;
                    }
                }
            }
        }

        private void FileIntegrity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            ComboBox comboBox = (ComboBox)sender;
            comboBox.Foreground = Brushes.Black;
            ComboBoxItem selectedItem = (ComboBoxItem)comboBox.SelectedItem;
            int cur_integ = 0, new_integ = 0;
            TestFiles ob = (TestFiles)FileDataGrid.Items[0];
            string cur_int = ob.integrity;
            var new_int = selectedItem.Content as string;

            foreach (KeyValuePair<int, string> keyValue in int_levels)
            {
                if (keyValue.Value == cur_int) cur_integ = keyValue.Key;
                if (keyValue.Value == new_int) new_integ = keyValue.Key;
            }
            
                string args = new_int.Substring(0, 2).ToUpper();                             //UN|LO|ME|HI|SI|PR добавить к строке аргументс
                
                /*изменить интегрити левел для файла*/
                /*файл можно взять из FileName.Text*/
                /*----------------------------*/
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                //startInfo.UseShellExecute = false;
                //startInfo.RedirectStandardOutput = true;
                startInfo.FileName = $"{curDir + "\\proc_cli.exe"}";
                //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
                startInfo.Arguments = $"-f {FileName.Text} -i {args}";   ////сюда
                startInfo.Verb = "runas";
                process.StartInfo = startInfo;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                /*----------------------------*/
                process.WaitForExit();
                if (process.ExitCode != 0)
                { comboBox.Text = cur_int; comboBox.Foreground = Brushes.Tomato; }
                else comboBox.Text = new_int;

        }

        private void File_apply_but_Click(object sender, RoutedEventArgs e)
        {

        }

        private void File_add_but_Click(object sender, RoutedEventArgs e)
        {
            string new_user = ComboBoxAddNewUser.Text;
            FilePrivDataGrid.Items.Clear();
                string sid = "";
                foreach (sid i in sid_list)
                {
                    if (i.username1 == new_user) { sid = i.sid1; break; }
                }
                var temp = new sid { sid1 = sid, username1 = new_user };
                FileSIDDataGrid.Items.Add(temp);
            List<access_list> new_acc_list = new List<access_list>();
            new_acc_list.Add(new access_list {name = "FILE_READ_DATA", value = false });
            new_acc_list.Add(new access_list { name = "FILE_WRITE_DATA", value = false });
            new_acc_list.Add(new access_list { name = "FILE_APPEND_DATA", value = false });
            new_acc_list.Add(new access_list { name = "FILE_READ_EA", value = false });
            new_acc_list.Add(new access_list { name = "FILE_WRITE_EA", value = false });
            new_acc_list.Add(new access_list { name = "FILE_EXECUTE", value = false });
            new_acc_list.Add(new access_list { name = "FILE_READ_ATTRIBUTES", value = false });
            new_acc_list.Add(new access_list { name = "FILE_WRITE_ATTRIBUTES", value = false });
            new_acc_list.Add(new access_list { name = "DELETE", value = false });
            new_acc_list.Add(new access_list { name = "READ_CONTROL", value = false });
            new_acc_list.Add(new access_list { name = "WRITE_DAC", value = false });
            new_acc_list.Add(new access_list { name = "WRITE_OWNER", value = false });
            new_acc_list.Add(new access_list { name = "SYNCHRONIZE", value = false });
            new_acc_list.Add(new access_list { name = "GENERIC_READ", value = false });
            new_acc_list.Add(new access_list { name = "GENERIC_WRITE", value = false });
            new_acc_list.Add(new access_list { name = "GENERIC_EXECUTE", value = false });

            TestFiles file = (TestFiles)FileDataGrid.Items[0];
            file.rights.Add(new ACLs { sid = sid, access = new_acc_list });
            //var new_ace = new ACLs
        }

        private void FileOwner_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string sid = "";
            var username = FileOwner.SelectedItem as string;
            foreach (sid i in sid_list)
            {
                if (i.username1 == username) { sid = i.sid1; break; }
            }

            

            /*изменить овнера для файла*/
            /*файл можно взять из FileName.Text, овнера из FileOwnerSID.Text*/
            /*----------------------------*/
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.UseShellExecute = false;
            //startInfo.RedirectStandardOutput = true;
            startInfo.FileName = "cmd";
            //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
            startInfo.Arguments = $"/c cd {curDir} & {curDir + "\\proc_cli.exe"} -f {FileName.Text} -ow {sid}";   ////сюда
            startInfo.Verb = "runas";
            process.StartInfo = startInfo;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.Start();
            /*----------------------------*/
            process.WaitForExit();

            FileOwnerSid.Text = sid;
        }

        private void FileName_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Enter)
            //{
            //    /*----------------------------*/
            //    System.Diagnostics.Process process = new System.Diagnostics.Process();
            //    System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //    startInfo.UseShellExecute = false;
            //    startInfo.RedirectStandardOutput = true;
            //    startInfo.FileName = "cmd";
            //    //startInfo.Arguments = "/c cd C:\\Users\\User\\Source\\Repos\\test_cli\\Release & C:\\Users\\User\\Source\\Repos\\test_cli\\Release\\test_cli.exe";
            //    startInfo.Arguments = "/c cd C:\\TOOLS\\tests & C:\\TOOLS\\tests\\proc_cli.exe -d";   ////сюда
            //    startInfo.Verb = "runas";
            //    process.StartInfo = startInfo;
            //    process.Start();
            //    /*----------------------------*/
            //}
        }
    }

    public class TestProcess
    {
        [JsonProperty("ASLR")]
        public bool ASLR { get; set; }
        [JsonProperty("DEP")]
        public bool DEP { get; set; }
        [JsonProperty("PID")]
        public int PID { get; set; }
        [JsonProperty("PPID")]
        public int PPID { get; set; }
        [JsonProperty("desc")]
        public string desc { get; set; }
        [JsonProperty("env")]
        public string env { get; set; }
        [JsonProperty("exe")]
        public string exe { get; set; }
        [JsonProperty("integrity")]
        public string integrity { get; set; }
        [JsonProperty("modules")]
        public List<string> modules { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("parent_name")]
        public string parent_name { get; set; }
        [JsonProperty("privileges")]
        public List<Process_priv> privileges { get; set; }
        [JsonProperty("type")]
        public string type { get; set;}
        
    }

    public class Process_priv
    {
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("value")]
        public bool value { get; set; }
    }

    public class Process_dll
    {
        public string DLLs { get; set; }
    }

    public class TestFiles
    {
        [JsonProperty("integrity")]
        public string integrity { get; set; }
        [JsonProperty("owner")]
        public string owner { get; set; }
        [JsonProperty("path")]
        public string path { get; set; }
        [JsonProperty("rights")]
        public List<ACLs> rights { get; set; }
    }

    public class ACLs
    {
        [JsonProperty("sid")]
        public string sid { get; set; }
        [JsonProperty("access")]
        public List<access_list> access { get; set; }
    }

    public class access_list
    {
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("value")]
        public bool value { get; set; }
    }

    public class Employee
    {
        public string Name { get; set; }
        public bool Gender { get; set; }
    }

    public class sid
    {
        public string sid1 { get; set; }
        public string username1 { get; set; }
    }
}
