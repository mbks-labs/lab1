﻿#include "get_file_info.h"
#include "get_proc_info.h"

#include <bitset>

std::string toString(FILEINT& fInt) {
	switch (fInt) {
	case F_NO:
		return ""; break;
	case F_UN:
		return "untrusted"; break;
	case F_LOW:
		return "low"; break;
	case F_MED:
		return "medium"; break;
	case F_HIGH:
		return "high"; break;
	case F_SYS:
		return "system"; break;
	case F_PR:
		return "protected"; break;
	}
	return "";
}

DWORD rightConvert(std::string right) {
	for (auto rights : rightsConvertor) {
		if (rights.second == right)
			return rights.first;
	}
	return 0;
}

std::string sidToString(PSID sid) {
	LPTSTR sidStr = NULL;
	if (!ConvertSidToStringSid(sid, &sidStr)) {
		printError(L"ConvertSidToStringSid");
		return "<error>";
	}
	std::string ret(sidStr);
	LocalFree(&sidStr);
	return ret;
}

PSID strToSid(std::string sid) {
	PSID psid = NULL;
	ConvertStringSidToSidA(sid.c_str(), &psid);
	return psid;
}

std::string sidToName(std::string sid) {
	PSID pSid;
	if (!ConvertStringSidToSid(sid.c_str(), &pSid)) {
		printError(L"ConvertStringSidToSid");
		return "<error>";
	}
	
	TCHAR lpName[256], lpDomain[256];
	DWORD nameSize = 256, domainSize = 256;
	SID_NAME_USE sidType;

	if (!LookupAccountSid(NULL, pSid, lpName, &nameSize, lpDomain, &domainSize, &sidType)) {
		printError(L"LookupAccountSid");
		return "<error>";
	}

	std::string ret = std::string(lpDomain) + "\\" + std::string(lpName);
	return ret;
}

FileInformation::FileInformation(std::string & filepath): filepath(filepath){
	this->fileHandle = CreateFile(
		filepath.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (fileHandle == INVALID_HANDLE_VALUE) {
		printError(L"CreateFile");
		return;
	}

	this->getOwnerAndDACL();
	this->getIntegrity();

	CloseHandle(this->fileHandle);
}

bool FileInformation::parseDACL(PACL dacl) {

	for (DWORD idx = 0; idx < dacl->AceCount; ++idx) {
		LPVOID pACE = NULL;
		if (!GetAce(dacl, idx, &pACE)) {
			printError(L"GetAce");
			return false;
		}
		PSID trustee = &((ACCESS_ALLOWED_ACE*)pACE)->SidStart;
		DWORD access = ((ACCESS_ALLOWED_ACE*)pACE)->Mask;
		this->rights.insert(std::make_pair(sidToString(trustee), access));
	}
	
	return true;
}

bool FileInformation::getOwnerAndDACL()
{
	DWORD errID = 0;
	SECURITY_INFORMATION desiredAccess = DACL_SECURITY_INFORMATION | GROUP_SECURITY_INFORMATION | OWNER_SECURITY_INFORMATION;
	PSID owner = NULL, group = NULL;
	PSECURITY_DESCRIPTOR secDesc = NULL;
	PACL fileDacl = NULL;
	if (ERROR_SUCCESS != (errID = GetSecurityInfo(this->fileHandle, SE_OBJECT_TYPE::SE_FILE_OBJECT, desiredAccess, &owner, &group, &fileDacl, NULL, &secDesc))) {
		printError(L"GetSecurityInfo", errID);
		return false;
	}
	
	this->ownerSid = sidToString(owner);
	this->parseDACL(fileDacl);

	/*LocalFree(&ownerSid);*/
	LocalFree(secDesc);
	return true;
}

bool FileInformation::getIntegrity()
{
	DWORD errID = 0;
	SECURITY_INFORMATION desiredAccess = LABEL_SECURITY_INFORMATION;
	PACL sacl = 0;
	PSECURITY_DESCRIPTOR secDesc = NULL;
	if (errID = GetSecurityInfo(this->fileHandle, SE_FILE_OBJECT, desiredAccess, 0, 0, 0, &sacl, &secDesc)) {
		printError(L"GetSecurityInfo", errID);
		return false;
	}
	if (!sacl) {
		this->integrity = FILEINT::F_NO;
		return true;
	}

	LPSTR buf = 0;
	ULONG strSize = 0;
	if (!ConvertSecurityDescriptorToStringSecurityDescriptorA(secDesc, SDDL_REVISION_1, desiredAccess, &buf, &strSize)) {
		printError(L"#SecDescConversion");
		return false;
	}

	std::string integrity(buf);
	std::string intLvl = integrity.substr(integrity.size() - 3, 2);
	if (intLvl == "LW")
		this->integrity = FILEINT::F_LOW;
	else if (intLvl == "ME")
		this->integrity = FILEINT::F_MED;
	else if (intLvl == "HI")
		this->integrity = FILEINT::F_HIGH;
	else if (intLvl == "SI")
		this->integrity = FILEINT::F_SYS;

	LocalFree(buf);
	return true;
}

void FileInformation::printInfo()
{
	std::cout <<
		"File: " << this->filepath << std::endl <<
		"Owner: " << sidToName(this->ownerSid) << "(SID: " << this->ownerSid << ")" << std::endl;

	std::cout << "Integrity: ";
	std::string intLvl;
	switch (this->integrity) {
	case F_LOW:
		intLvl = "low";
		break;
	case F_MED:
		intLvl = "medium";
		break;
	case F_HIGH:
		intLvl = "high";
		break;
	case F_SYS:
		intLvl = "system";
		break;
	case F_NO:
		intLvl = "not set";
		break;
	}
	std::cout << intLvl << std::endl;

		std::cout << "Rights:" << std::endl;
	for (auto acePair : this->rights) {		
		std::cout << "SID: " << acePair.first << "\t Name: " << sidToName(acePair.first) << "\n\t Rights: 0b" << std::bitset<32>(acePair.second) << ": " << std::endl;
		for (auto rightsPair : rightsConvertor) {
			if (acePair.second & rightsPair.first)
				std::cout << "\t" << rightsPair.second << std::endl;
		}
		std::cout << std::endl;
	}

}

jsonxx::Array FileInformation::toJSON()
{
	jsonxx::Object json;
	json << "path" << this->filepath;
	json << "owner" << this->ownerSid;
	json << "integrity" << toString(this->integrity);
	jsonxx::Array rights;

	for (auto rightsPair : this->rights) {
		jsonxx::Object sidAndRight;
		sidAndRight << "sid" << rightsPair.first;
		jsonxx::Array right;
		for (auto rConv : rightsConvertor) {
			jsonxx::Object rObj;
			rObj << "name" << rConv.second;
			rObj << "value" << ((rightsPair.second & rConv.first) ? true : false);
			right << rObj;
		}
		sidAndRight << "access" << right;
		rights << sidAndRight;
	}

	json << "rights" << rights;	
	jsonxx::Array ret; ret << json;
	return ret;
}

bool FileInformation::setOwner(std::string newOwnerSID)
{
	this->fileHandle = CreateFile(
		filepath.c_str(),
		GENERIC_READ | WRITE_OWNER,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	PSID SID;
		
	if (!ConvertStringSidToSidA(newOwnerSID.c_str(), &SID)) {
		printError(L"ConvertStringSidToSidA");
		return false;
	}
	
	if (fileHandle == INVALID_HANDLE_VALUE) {
		printError(L"CreateFile");
		return false;
	}

	DWORD res = SetSecurityInfo(this->fileHandle, SE_FILE_OBJECT, OWNER_SECURITY_INFORMATION, SID, NULL, NULL, NULL);
	if (res != ERROR_SUCCESS) {
		printError(L"SetSecurityInfo (owner)", res);
		return false;
	}

	CloseHandle(this->fileHandle);

	return true;
}

bool FileInformation::setIntegrity(FILEINT intLvl)
{
	std::string intStr = "S:(ML;;NW;;;";
	switch(intLvl) {
	case F_LOW:
		intStr += "LW)"; break;
	case F_MED:
		intStr += "ME)"; break;
	case F_HIGH:
		intStr += "HI)"; break;
	case F_SYS:
		intStr += "SI)"; break;
	}

	this->fileHandle = CreateFile(
		filepath.c_str(),
		GENERIC_READ | WRITE_OWNER,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	DWORD dwErr = ERROR_SUCCESS;
	PSECURITY_DESCRIPTOR pSD = NULL;
	PACL pSacl = NULL; // not allocated
	BOOL fSaclPresent = FALSE;
	BOOL fSaclDefaulted = FALSE;
	if (!ConvertStringSecurityDescriptorToSecurityDescriptorA(
		intStr.c_str(), SDDL_REVISION_1, &pSD, NULL)) {
		printError(L"ConvertStringSDToSD");
		return false;
	}

	if (GetSecurityDescriptorSacl(pSD, &fSaclPresent, &pSacl,
									&fSaclDefaulted))
	{
		// Note that psidOwner, psidGroup, and pDacl are 
		// all NULL and set the new LABEL_SECURITY_INFORMATION
		dwErr = SetSecurityInfo(this->fileHandle,
										SE_FILE_OBJECT, LABEL_SECURITY_INFORMATION,
										NULL, NULL, NULL, pSacl);
	}
	LocalFree(pSD);

	if(dwErr != ERROR_SUCCESS)
		return false;

	return true;
}

bool FileInformation::setRights(std::string sid, DWORD rights, bool setNotRemove)
{
	DWORD dwRes = 0;
	PACL oldDacl = NULL, newDacl = NULL;
	PSECURITY_DESCRIPTOR pSD = NULL;
	EXPLICIT_ACCESS_A ea;

	this->fileHandle = CreateFile(
		filepath.c_str(),
		GENERIC_READ | WRITE_DAC,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	dwRes = GetSecurityInfo(this->fileHandle, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, NULL, NULL, &oldDacl, NULL, &pSD);
	if (dwRes != ERROR_SUCCESS) {
		printError(L"GetSecurityInfo", dwRes);
		if (pSD != NULL) LocalFree(pSD);
		if (newDacl != NULL) LocalFree(newDacl);
		return false;
	}

	try {
		if (setNotRemove)
			rights |= this->rights.at(sid);
		else
			rights ^= this->rights.at(sid);
	}
	catch(...){}

	ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS_A));
	ea.grfAccessPermissions = rights;
	ea.grfAccessMode = ACCESS_MODE::SET_ACCESS;
	ea.grfInheritance = NO_INHERITANCE;
	ea.Trustee.TrusteeForm = TRUSTEE_FORM::TRUSTEE_IS_SID;
	ea.Trustee.ptstrName = (LPTSTR)(strToSid(sid.c_str()));

	dwRes = SetEntriesInAcl(1, &ea, oldDacl, &newDacl);
	if (dwRes != ERROR_SUCCESS) {
		printError(L"SetEntriesInAcl", dwRes);
		if (pSD != NULL) LocalFree(pSD);
		if (newDacl != NULL) LocalFree(newDacl);
		return false;
	}

	dwRes = SetSecurityInfo(this->fileHandle, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, NULL, NULL, newDacl, NULL);
	if (dwRes != ERROR_SUCCESS) {
		printError(L"GetSecurityInfo", dwRes);
		if (pSD != NULL) LocalFree(pSD);
		if (newDacl != NULL) LocalFree(newDacl);
		return false;
	}

	return true;
}
