#include <string>
#include <vector>
#include <iostream>
#include <filesystem>
#include <locale>

namespace fs = std::filesystem;

const std::string psexec("PsExec64.exe");

bool checkPSExec() {
	auto curDir = fs::current_path();
	for (auto elem : fs::directory_iterator(curDir)) {
		if (elem.path().filename() == psexec)
			return true;
	}
	return false;
}

std::string locateCore() {
	auto curDir = fs::current_path();
	curDir += "/core.exe";
	return curDir.generic_string();
}

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "ru-RU");
	if (!checkPSExec()) {
		std::cout << "Can't locate PsExec64.exe in current path. Aborting.." << std::endl;
		return -1;
	}

	std::vector<std::string> args(argv + 1, argv + argc);
	std::string executor = psexec + " -nobanner -s \"" + locateCore()  + "\"";
	for (auto arg : args) {
		executor += " " + arg;
	}
		
	system(executor.c_str());
	return 0;
}