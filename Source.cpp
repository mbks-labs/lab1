﻿#include "ErrorHandler.h"
#include "get_proc_info.h"
#include "get_file_info.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <bitset>

#pragma comment(lib, "Version.lib")

std::vector<ProcessInformation> procs;

void GetProcessList()
{
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		printErrorAndFail(L"CreateToolhelp32Snapshot");
	}

	// Set the size of the structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process,
	// and exit if unsuccessful
	if (!Process32First(hProcessSnap, &pe32))
	{
		CloseHandle(hProcessSnap);          // clean the snapshot object
		printErrorAndFail(L"Process32First"); // show cause of failure
	}

	do {
		auto newProc = ProcessInformation(pe32);
		if (newProc.isValid())
			procs.push_back(newProc);
	} while (Process32Next(hProcessSnap, &pe32));

	CloseHandle(hProcessSnap);
}

const std::unordered_map<std::string, std::string> privilegesConvertor = {
	{"assign_primary_token", SE_ASSIGNPRIMARYTOKEN_NAME},
	{"audit", SE_AUDIT_NAME},
	{"backup", SE_BACKUP_NAME},
	{"change_notify", SE_CHANGE_NOTIFY_NAME},
	{"create_global", SE_CREATE_GLOBAL_NAME},
	{"create_pagefile", SE_CREATE_PAGEFILE_NAME},
	{"create_permanent", SE_CREATE_PERMANENT_NAME},
	{"create_symlink", SE_CREATE_SYMBOLIC_LINK_NAME},
	{"create_token", SE_CREATE_TOKEN_NAME},
	{"debug", SE_DEBUG_NAME},
	{"delegate_session_user_impersonate", SE_DELEGATE_SESSION_USER_IMPERSONATE_NAME},
	{"enable_delegation", SE_ENABLE_DELEGATION_NAME},
	{"impersonate",	SE_IMPERSONATE_NAME},
	{"inc_base_priority",	SE_INC_BASE_PRIORITY_NAME},
	{"inc_quota",	SE_INCREASE_QUOTA_NAME},
	{"inc_working_set",	SE_INC_WORKING_SET_NAME},
	{"load_driver",	SE_LOAD_DRIVER_NAME},
	{"lock_memory",	SE_LOCK_MEMORY_NAME},
	{"machine_account",	SE_MACHINE_ACCOUNT_NAME},
	{"manage_volume",	SE_MANAGE_VOLUME_NAME},
	{"prof_single_process",	SE_PROF_SINGLE_PROCESS_NAME},
	{"relabel",	SE_RELABEL_NAME},
	{"remote_shutdown",	SE_REMOTE_SHUTDOWN_NAME},
	{"restore",	SE_RESTORE_NAME},
	{"security",	SE_SECURITY_NAME},
	{"shutdown",	SE_SHUTDOWN_NAME},
	{"sync_agent", SE_SYNC_AGENT_NAME},
	{"sys_env",	SE_SYSTEM_ENVIRONMENT_NAME},
	{"sys_profile", SE_SYSTEM_PROFILE_NAME},
	{"systemtime", SE_SYSTEMTIME_NAME},
	{"take_ownership",	SE_TAKE_OWNERSHIP_NAME},
	{"tcb",	SE_TCB_NAME},
	{"time_zone",	SE_TIME_ZONE_NAME},
	{"trusted_credman",	SE_TRUSTED_CREDMAN_ACCESS_NAME},
	{"undock",	SE_UNDOCK_NAME},
	{"unsolicited_input", SE_UNSOLICITED_INPUT_NAME},
};

void selfGrantAllPrivileges() {
	HANDLE token;
	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &token)) {
		printError(L"OpenProcessToken ##");
		return;
	}

	for (auto kv : privilegesConvertor) {
		SetPrivilege(token, kv.second.c_str(), TRUE);
	}
}

void printUsage(std::string progName) {
	std::cout << "Usage: " << progName << "[-h] [<-d> [-o jsonfile]] [<-p pid> [-g] [-i UN|LO|ME|HI|SY|PR] [[-se] privilege1:state1 privilege2:state2 ...] [<-f file> [-g] [-ow ownersid] [-i LO|ME|HI|SI] [-ra SID right1 right2...]" << std::endl <<
		"Options:" << std::endl <<
		"\t" << "-h -- show help" << std::endl <<
		"\t" << "-d -- dump information about processes to json file. Output file can be specified with -o options" << std::endl <<
		"\t" << "-o file -- specify .json file to output" << std::endl <<
		"\t" << "-f file -- specify file to get information about it" << std::endl <<
		"\t" << "-g -- get information" << std::endl <<
		"\t" << "-ow owner-- set file owner" << std::endl <<
		"\t" << "-ra SID righ1 right2 ... - add specified rights to SID ACE at file DACL" << std::endl <<
		"\t" << "-rr SID right1 right2 ... - remove rights to SID ACE at file DACL" << std::endl << 
		"\t" << "-p pid -- specify process ID to modify information" << std::endl <<
		"\t" << "-i UN|LO|ME|HI|SY|PR -- set integrity level to untrusted|low|medium|high|system|protected level" << std::endl <<
		"\t" << "-se privilege -- give or take privilege" << std::endl <<
		"\t" << "  List of privileges:" << std::endl;
		for (auto& kv : privilegesConvertor) {
			std::cout << "\t\t" << kv.second << std::endl;
		}
		std::cout << std::endl;
		std::cout << "\t  List of rights:" << std::endl;
		for (auto& kv : rightsConvertor) {
			std::cout << "\t\t" << kv.second << std::endl;
		}
}

void dumpInfo(std::ofstream& jsonFile) {
	std::cout << "Saving information..." << std::endl;

	GetProcessList();
	jsonxx::Array jsonProcs;
	for (auto& proc : procs) {
		jsonProcs << proc.toJSON();
	}
	jsonFile << jsonProcs;
	jsonFile.close();

	std::cout << "Information about processes saved successfully!" << std::endl;
}

std::string getDir() {
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string name(buffer);
	auto pos = name.find_last_of("\\/");
	return name.substr(0, pos);
}

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "ru-RU");
	if (argc < 2 || !strncmp(argv[1], "-h", 3)) {
		printUsage(argv[0]);
		return 0;
	}

	selfGrantAllPrivileges();

	bool procProcessing = false;
	std::ofstream jsonFile;
	bool dump = false;
	bool integritySet = false;
	bool privilegeChange = false;
	int pid = -1;
	PROCINTEGRITY integrityToSet;
	std::string integrityToSetOrig;
	std::unordered_map<std::string, bool> privilegesToSet;
	
	std::string fFilepath;
	bool fGet = false;
	std::string fOwnerSID;
	bool fSetOwner = false;
	bool fileProcessing = false;
	FILEINT fIntegritySet = F_NO;
	std::string fIntegrityStr;
	bool fIntegrity = false;
	std::string rightsSid = "";
	bool fSetRights = false;
	bool fRemoveRights = false;
	std::vector<DWORD> rightsToSet;
	std::vector<DWORD> rightsToRemove;

	int failedActions = 0;

	std::vector<std::string> args(argv, argv + argc);
	for (int i = 1; i < args.size(); ++i) {
		if (args[i] == "-h") {
			printUsage(argv[0]);
			return 0;
		}
		if (args[i] == "-d")
			dump = true;
		if (args[i] == "-o" && i < args.size() - 1) {
			std::string file = getDir() + "\\" + args[i + 1];
			std::cout << "Opening file " << file << "..." << std::endl;
			jsonFile.open(file, std::ofstream::out | std::ofstream::trunc);
			if (!jsonFile.is_open()) {
				std::cout << "Can't open file " << args[i + 1] << std::endl;
				return -1;
			}
		}
		if (args[i] == "-p" && i < args.size() - 1) {
			procProcessing = true;
			try {
				pid = std::stoi(args[i + 1]);
			}
			catch (std::exception ex) {
				std::cout << "-p: invalid argument";
				if (jsonFile.is_open())
					jsonFile.close();
				return -1;
			}
		}
		if (args[i] == "-i" && i < args.size() - 1) {
			std::string integrity = args[i + 1];
			if (procProcessing) {
				if (integrity == "UN")
					integrityToSet = PROCINTEGRITY::UNTRUSTED_INTEGRITY;
				else if (integrity == "LO")
					integrityToSet = PROCINTEGRITY::LOW_INTEGRITY;
				else if (integrity == "ME")
					integrityToSet = PROCINTEGRITY::MEDIUM_INTEGRITY;
				else if (integrity == "HI")
					integrityToSet = PROCINTEGRITY::HIGH_INTEGRITY;
				else if (integrity == "SY")
					integrityToSet = PROCINTEGRITY::SYSTEM_INTEGRITY;
				else if (integrity == "PR")
					integrityToSet = PROCINTEGRITY::PROTECTED_INTEGRITY;
				else
					integrityToSet = PROCINTEGRITY::UNKNOWN_INTEGRITY;
				integritySet = true;
			}
			else if (fileProcessing) {
				fIntegrity = true; 
				fIntegrityStr = integrity;
				if (integrity == "UN")
					fIntegritySet = FILEINT::F_UN;
				else if (integrity == "LO")
					fIntegritySet = FILEINT::F_LOW;
				else if (integrity == "ME")
					fIntegritySet = FILEINT::F_MED;
				else if (integrity == "HI")
					fIntegritySet = FILEINT::F_HIGH;
				else if (integrity == "SY")
					fIntegritySet = FILEINT::F_SYS;
				else if (integrity == "PR")
					fIntegritySet = FILEINT::F_PR;
			}
		}
		if (args[i] == "-se" && i < args.size() - 1) {
			privilegeChange = true;
			for (int j = i+1; j < args.size(); j++) {
				std::string privAndVal = args[j];
				auto dots = privAndVal.find(":");
				std::string priv = privAndVal.substr(0, dots);
				std::string val = privAndVal.substr(dots + 1);
				try {
					privilegesToSet.insert(std::make_pair(privilegesConvertor.at(priv), val == "true"));
				}
				catch (std::exception ex) {
					try {
						privilegesToSet.insert(std::make_pair(priv, val == "true"));
					}
					catch(...){
						std::cout << "-se: can't find such privilege: " << priv << std::endl;
						if (jsonFile.is_open())
							jsonFile.close();
						return -1;
					}
				}
			}
		}

		if (args[i] == "-f" && i < args.size() - 1) {
			fileProcessing = true;
			fFilepath = args[i + 1];
		}
		if (args[i] == "-g") {
			fGet = true;
		}
		if (args[i] == "-ow" && i < args.size() - 1) {
			fSetOwner = true;
			fOwnerSID = args[i + 1];
		}
		if (args[i] == "-ra" && i < args.size() - 2) {
			rightsSid = args[i + 1];
			fSetRights = true;
			for (int j = i + 2; j < args.size(); j++) {
				std::string right = args[j];
				DWORD dwRight = rightConvert(right);
				if (dwRight) {
					rightsToSet.push_back(dwRight);
				}
			}
		}
		if (args[i] == "-rr" && i < args.size() - 2) {
			rightsSid = args[i + 1];
			fRemoveRights = true;
			for (int j = i + 2; j < args.size(); j++) {
				std::string right = args[j];
				DWORD dwRight = rightConvert(right);
				if (dwRight)
					rightsToRemove.push_back(dwRight);
			}
		}

	}
	
	if (dump) {
		if (!jsonFile.is_open()) {
			jsonFile.open(getDir() + "/procs.json");
		}
		dumpInfo(jsonFile);

		std::cout << "ACTIVE PROCESSES:" << std::endl << std::setw(49) << std::setfill('=') << "=" << std::setfill(' ') << std::endl;
		for (auto proc : procs) {
			proc.printInfo();
		}
	}

	if (privilegeChange) {
		auto process = ProcessInformation(pid);
		for (auto priv : privilegesToSet) {
			std::cout << "Trying to set for process " << pid << " privilege " << priv.first << " to value " << (priv.second ? "true" : "false") << std::endl;

			if (process.setPrivilege(priv.first, priv.second)) {
				std::cout << "Successfully set!" << std::endl;
			}
			else {
				std::cout << "Setting privilege wasn't successfull" << std::endl;
				failedActions++;
			}
		}
	}

	if (integritySet) {
		std::cout << "Trying to set for process " << pid << " integrity " << integrityToSetOrig << std::endl;
		auto process = ProcessInformation(pid);
		if (process.setIntegrity(integrityToSet))
			std::cout << "Successfully set!" << std::endl;
		else{
			std::cout << "Setting integrity wasn't successfull" << std::endl;
			failedActions++;
		}
	}

	if (fileProcessing && fGet) {
		FileInformation fInfo = FileInformation(fFilepath);
		std::ofstream fInfoFile;
		fInfoFile.open(getDir() + "\\file.json");
		fInfoFile << fInfo.toJSON();
		fInfo.printInfo();
		fInfoFile.close();
	}

	if (fileProcessing && fSetOwner) {
		std::cout << "Trying to set new owner of file " << fFilepath << " with SID " << fOwnerSID << std::endl;
		FileInformation fInfo = FileInformation(fFilepath);
		if (fInfo.setOwner(fOwnerSID))
			std::cout << "Successfully set!" << std::endl;
		else{
			std::cout << "Setting owner wasn't successfull" << std::endl;
			failedActions++;
		}
	}

	if (fileProcessing && fIntegrity) {
		std::cout << "Trying to set integrity of " << fFilepath << " to " << fIntegrityStr << std::endl;
		FileInformation fInfo = FileInformation(fFilepath);
		if(fInfo.setIntegrity(fIntegritySet))
			std::cout << "Successfully set!" << std::endl;
		else{
			std::cout << "Setting integrity wasn't successfull" << std::endl;
			failedActions++;
		}
	}

	if (fileProcessing && fSetRights) {
		auto fInfo = FileInformation(fFilepath);
		DWORD rights = 0;
		for (auto right : rightsToSet) {
			rights |= right;
		}
		std::cout << "Trying to set user with SID " << rightsSid << " rights to " << std::bitset<32>(rights) << std::endl;
		if(fInfo.setRights(rightsSid, rights))
			std::cout << "Successfully set!" << std::endl;
		else{
			std::cout << "Setting rights wasn't successfull" << std::endl;
			failedActions++;
		}
	}

	if (fileProcessing && fRemoveRights) {
		auto fInfo = FileInformation(fFilepath);
		DWORD rights = 0;
		for (auto right : rightsToRemove) {
			rights |= right;
		}
		std::cout << "Trying to remove user with SID " << rightsSid << " rights to " << std::bitset<32>(rights) << std::endl;
		if (fInfo.setRights(rightsSid, rights, false))
			std::cout << "Successfully set!" << std::endl;
		else{
			std::cout << "Setting rights wasn't successfull" << std::endl;
			failedActions++;
		}
	}

	return failedActions;
}