﻿#ifndef _ERR_HANDLER_H
#define _ERR_HANDLER_H

#include "WinHeaders.h"
#include <strsafe.h>

#include <iostream>

void printError(std::wstring, DWORD dwErr = GetLastError());
void printErrorAndFail(std::wstring, DWORD dwErr = GetLastError());

#endif
