﻿#include "ErrorHandler.h"


void printError(std::wstring func, DWORD dwErr) {
#ifndef _DEBUG
	return;
#else
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dwErr,
		MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, (lstrlen((LPCTSTR)lpMsgBuf) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("Error %d: %s"), dwErr, lpMsgBuf);

	std::wcout << "Error in function " << func << std::endl << (LPTSTR)lpDisplayBuf << std::endl;

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
#endif
}

void printErrorAndFail(std::wstring func, DWORD dwErr) {
#ifndef _DEBUG
	return;
#else
	printError(func, dwErr);

	exit(-1);
#endif
}