﻿#pragma once

#include "WinHeaders.h"

#include <string>
#include <vector>
#include <unordered_map>

enum PROCTYPE {
	_TYPE_NO_VAL,
	x86, x64, OTHER
};

enum PROCENV {
	_ENV_NO_VAL,
	NATIVE, CLR
};

enum PROCINTEGRITY {
	_INT_NO_VAL,
	UNKNOWN_INTEGRITY,
	UNTRUSTED_INTEGRITY,
	LOW_INTEGRITY,
	MEDIUM_INTEGRITY,
	HIGH_INTEGRITY,
	SYSTEM_INTEGRITY,
	PROTECTED_INTEGRITY
};

const std::unordered_map<PROCINTEGRITY, std::string> integritySIDs = {
	{UNTRUSTED_INTEGRITY, "S-1-16-0"},
	{LOW_INTEGRITY, "S-1-16-4096"},
	{MEDIUM_INTEGRITY, "S-1-16-8192"},
	{HIGH_INTEGRITY, "S-1-16-12288"},
	{SYSTEM_INTEGRITY, "S-1-16-16384"},
	{PROTECTED_INTEGRITY, "S-1-16-20480"},
};

class ProcessInformation {

	bool initialized;

	HANDLE processHandle;		

	std::string processName;	//+	+
	std::string processDesc;	//+	+
	DWORD processID;			//+	+
	std::string processExePath;	//+	+
	std::string ownerSID;

	std::string processParentName;	//+ +
	DWORD processParentID;		//+	+

	PROCTYPE processType;		//+	+
	PROCENV processEnvironment;	//+	+

	bool processUseDEP;			//+	+
	bool processUseASLR;		//+ +

	std::vector<std::string> processModules;	//+

	PROCINTEGRITY processIntegrityLevel;	//+

	bool findExePath();
	bool findDescription();
	bool findParentName();
	bool findType();
	bool findEnv();
	bool findDEPPolicy();
	bool findASLRPolicy();
	bool findModules();
	bool findIntegrity();
	bool findPrivileges();
	bool findOwner();

	std::vector<std::pair<LUID, bool> > processPrivileges;

	explicit ProcessInformation();
public:
	~ProcessInformation();
	explicit ProcessInformation(PROCESSENTRY32 pe32);
	ProcessInformation(int PID);
	void printInfo();
	bool isValid() { return initialized; }

	bool setIntegrity(PROCINTEGRITY intLvl);
	bool setPrivilege(std::string se_privilege, bool value);

	std::string getName();
	unsigned int getPid();
	std::string getPath();
	
	jsonxx::Object toJSON();
};

BOOL SetPrivilege(
	HANDLE hToken,          // access token handle
	LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
	BOOL bEnablePrivilege   // to enable or disable privilege
);